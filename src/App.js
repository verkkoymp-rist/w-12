import React from 'react';
import './BusinessCard.css'; // Import the CSS file for styling

function App() {
  return (
    <div className="business-card">
      <img src={process.env.PUBLIC_URL + '/logo.png'} alt="Company Logo" className="business-logo" />
      <span className="company-name">VAMK</span>
      <span className="company-desc">University of Applied Sciences</span>
      <span className="name">Niilo Asikainen</span>
      <span className="phone">+358 40 484 3771</span>
      <span className="address">Wolffintie 30, FI-65200 VAASA, Finland</span>
    </div>
  );
}

export default App;
